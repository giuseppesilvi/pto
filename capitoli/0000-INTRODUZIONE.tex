%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../2020-PTO.tex

%-------------------------------------------------------------------------------
%---------------------------------------------------------------- INTRODUZIONE -
%-------------------------------------------------------------------------------
\begin{refsection}
\chapter*{Introduzione}

\begin{flushright}
  \textit{
    Nella nostra anima c'\`e\\
    una incrinatura che, se sfiorata, \\
    risuona come un vaso prezioso\\
    riemerso dalle profondit\`a	della terra} \cite{kandinsky:spirituale}
\end{flushright}

\vfill

Ho desiderato, e a lungo provato, di trovarmi nella circostanza di una ricerca
scientifica, istituzionalizzata. Mi sono portato in sala anecoica\footnote{
\emph{IRCAM}, 2018. Con l'aiuto e la presenza fondamentale di \mmm~(amico,
compositore, ricercatore indipendente), ho passato una settimana nel centro
parigino occupando la sala anecoica, ospite di \jfr, allora ricercatore
\emph{IRCAM}. Il viaggio e l'alloggio furono auto-sostenuti, auto-prodotti, allo
scopo di testare e rilevare dati sugli altoparlanti \emph{S.T.ONE} e il neonato
\emph{S.T.ON3L} (nato la notte prima di partire, dopo una lunga gestazione.)}.
Ho ripetutamente ed inutilmente cercato il supporto di un'istituzione\footnote{
Dottorati e residenze, una decina tra il 2016 e il 2019}. Ho fallito l'approccio
ad un finanziamento europeo\footnote{Nel 2018 attraverso un bando di
\emph{LazioInnova} è nata \emph{Spherical Technologies SRLS}, con lo scopo di
sviluppare commercialmente le ricerche sui dispositivi sferici di ripresa e
diffusionema fino ad allora sviluppati. Ma l'imprenditore che è in me è un
incapace totale ed il progetto non ha portato altro che spese aggiuntive sotto
il capitolo \emph{costi di istituzionalizzazione}, perdendo quindi l'accesso ai
fondi per lo sviluppo}. Ho cercato continuamente \emph{qualcosa} che potesse
sollevare lo stato dell'arte della mia ricerca dal livello domestico in cui è
nata ad un livello scientifico, istituzionalizzato. Ero convinto che per parlare
di temi come lo spazio sonoro, l'ascolto e la produzione di strumenti in grado
di manipolarli, si dovesse necessariamente passare per un percorso di
divulgazione scientifica riconosciuta. In quel modo, certificandomi, mi serei
sentito parte di una comunità. Ho osservato i miei continui fallimenti,
motivandoli, laddove mi è stato possibile capire. Ho compreso una verità del
mio tempo contemporaneo, delle istituzioni e del mondo accademico-scientifico,
sulla quale proverò a scrivere più accurataemtne nelle conclusioni di questo
percorso esplicativo.

\begin{figure}[H]
	\begin{center}
    \includegraphics[width=1\textwidth]{disegni/grafico-relazioni.jpg}
    %\vspace{-50pt}
  \caption{\textbf{Relazioni}, il contesto.}
\end{center}
\end{figure}

Ad un certo punto ho iniziato ad osservare quello che avevo fatto nel contesto
in cui tutto è nato, la sala da concerto. Il contesto, la circostanza, alla luce
di questo nuovo sguardo, è passato da essere sfondo, distante e sbiadito, a
superficie, piano complesso su cui ho disegnato i miei poli e i miei zeri. Mi
sono visto filtro di una struttura sonora che si staglia lungo la storia della
scuola elettroacustica romana, mi sono reso conto che il mio luogo non poteva
che essere quello, una sala da concerto e non un'istituzione in grado di
dialogare, e quindi comprendere e parlare, la mia stessa lingua. E non perchè
non lo volessi, ma perché ho riconosciuto che, nella storia di cui mi sono visto
parte, questo non è mai accaduto, questo luogo non c'è mai stato. Un luogo dove
tutto è cresciuto senza alcun tipo di supporto, fatta esclusione del sostegno
diretto di qualche caro amico che finirà, in un modo o nell'altro, tra queste
pagine.

Tra le mura del \emph{Conservatorio S. Cecilia}, dove non potrà mai esserci una
sala anecoica, c'è una delle sale da concerto più belle d'Italia (non è
oggettivamente vero, ma poco importa, come spesso accade per la bellezza). In
quei luoghi sono cresciuti i miei orecchi e la mia percezione ha catturato
particolari affinché fossi in grado di comporre, nel tempo, un pensiero generale.
In quel pensiero c'è stato spazio per la creazione musicale, quella tipica
della scuola in cui sono nato, direttamente con le parole dei miei maestri:
una \emph{scuola del fare} (\gn), nel suo rapporto complesso tra
\emph{progetto-strumento-opera} (\ml).

\begin{figure}[t!]
	\begin{center}
    \includegraphics[width=1\textwidth]{storia/24574_1394353980838_3108209_n-bn.jpeg}
    %\vspace{-50pt}
  \caption{\textbf{Monolite}, al suo fianco un'asta e dei microfoni. \ps.}
\end{center}
\end{figure}

Lo studio delle relazioni tra spazio acustico, rito del concerto, strumento,
interprete, pubblico e compositore hanno permesso lo sviluppo di una ricerca
musicale articolata e complessa, domestica ma non per questo meno rigorosa di
una ricerca scientifica istituzionalizzata, non per questo inutile.

\emph{Pensare tetraedrico oggi} significa guardare al pensiero di \adb~ e \mg~ e
cercare di capire cosa ne è rimasto nel pensiero altrui. Non scoraggiarsi davanti
all'abuso di superficialità a cui sono sottoposte parole come ascolto, suono,
stereofonia. Non limitarsi al collegamento \emph{Gerzon-ambisonic}, o agli epiloghi
solipsistici proposti nelle realtà virtuali, che non rivendicano senso di
appartenenza a quell'elegante modo di descrivere gli spazi sonori delle infinite
realtà acustiche attraverso il concetto delle armoniche sferiche. Non limitarsi
all'idea di una tecnologia che si pone solo al servizio di un fine, ormai
esclusivamente ludico e di intrattenimento, ma che possa essere il mezzo stesso
di speculazione sulle potenzialità tecniche reali del produrre e riprodurre
suoni. Si tratta, in fondo, e fin dal principio, di un pensiero estremamente
musicale.

A questo e di questo scrivo, alla possibilità reale di portare avanti una
ricerca, destinarla ad un pubblico, divulgando il pensiero musicale che l'ha
permessa e che la sostiene. Ho visto crescere la ricerca e l'interesse attorno ad
essa, e seppur non avesse tutti i requisiti per stabilire una validità scientifica
aveva la dignità di fatto, di aver fatto i conti con la musica (nella sua
polimorfa struttura di tempo, spazio, memoria). Non una situazione anecoica,
\emph{che non dà luogo a fenomeni di eco}, ma la realtà acustica del vivo
musicale è stato il banco di prova e l'ambiente di ricerca del mio pensiero e su
questa mi auguro di avere confronto.

%-------------------------------------------------------------------------------
%------------------------------------------------------------ PERCORSO STORICO -
%-------------------------------------------------------------------------------

\section*{Percorso storico}

L'uomo che più di tutti avrebbe sorriso alle evoluzioni delle mie stravaganti
idee sullo spazio sonoro sarebbe stato \ps. Avrebbe sorriso, consapevole di
esserne stato la causa.

\emph{Piero} è stato docente di \emph{Elettroacustica} presso il \sccons~di Roma
dal 2002 al 2012. È lì che l'ho conosciuto, nel 2007. Le sue lezioni erano
così belle che la loro bellezza ti raggiungeva prima ancora di iniziare i suoi
corsi. Gli altri studenti ne parlavano come di un personaggio mitologico, dalla
sapienza teorica seconda solo all'appassionata pratica tecnica. Si creò tra noi,
in un tempo piccolo, un rapporto speciale. Il nostro grado comune era l'\amb, anello
che lui stesso aveva forgiato. Quando Piero iniziò a parlarci di \amb~fu per noi
un momento di alta formazione perché di fatto partecipavamo, nell'unica realtà
del presente, alle sue ricerche. La materia di cui lui ci narrava le gesta era
argilla fresca tra le sue mani. Iniziammo a lavorare attivamente alle teorie di
\mg~nel 2008, mettendo in pratica e simulando molti dei test di ascolto
che egli suggeriva. Era questo il metodo di Piero, ascoltare per capire e, poi,
credere. Un metodo didattico che vide il suo picco di bellezza durante le
giornate di registrazione comparata tra configurazioni microfoniche
\emph{stereo} o \emph{surround}.

\begin{figure}[t!]
	\begin{center}
    \includegraphics[width=1\textwidth]{storia/DSC_5748.jpg}
    %\vspace{-50pt}
  \caption{\textbf{Surround Journey 2}, un esemplare di Fukada Tree realizzato
  con un profilato in alluminio ultralggero (una finestra).}
\end{center}
\end{figure}

Nel 2008 nacque \emufest\footnote{\emph{Festival Internazionale di
Musica Elettronica} di Roma. Nato all'interno del \sccons~dal lavoro concertato
dell'allora direttore \es, \gn~e \ps, con il supporto di molti docenti del
conservatorio stesso, \emufest~è stato uno dei festival di riferiemento mondiale
tra il 2008 e il 2017 quando, celebrandone la decima edizione, ne è stata
dichiarata anche la fine politica, non musicale.}. \emufest~fu il prototipo di
didattica aumentata, il \emph{fare} della scuola romana portato oltre ogni
aspettativa, che trascinò noi, gli studenti, dentro la musica e non verso la
musica. Con circa cento
brani l'anno provenienti da tutto il mondo e suonati in concerti da studenti,
docenti e professionisti esterni è stata, a mio avviso, una rarissima forma di
didattica reale, possibile e necessaria, l'unica che questo contesto artistico
e disciplinare dovrebbe avere. Nel 2009, per la seconda edizione del festival,
realizzai per Piero il software che ci avrebbe permesso di utilizzare la
tecnologia \amb~nella sala da concerto del \sccons.

Del 2009 fu anche la prima sessione di registrazione comparata di configurazioni
stereofoniche a cui ho partecipato\footnote{15 aprile 2009}. Nel 2010 facemmo
visita con una piccola delegazione di studenti (con me, Federico Scalas,
Leonardo Zaccone, Simone Pappalardo e lo stesso Piero) presso la \emph{Casa del
Suono} di Parma dove ascoltammo le parole di \fa, i suoni della \emph{Sala Bianca}
in \emph{WFS} ed alcune registrazioni fatte da noi in \amb. L'incontro con
\emph{Fons} portò ad un livello superiore il nostro lavoro con l'\amb, con la
consapevolezza che la strada che stavamo percorrendo era ormai la nostra.

Non un ambiente istituzionale dal rigore scientifico, ma un laboratorio dove la
mente e le braccia lavorano rigorosamente al servizio della musica. Da qui viene
il mio pensiero ed il mio modo di fare. Da qui posso partire con serenità e
spiegare la mia attuale visione della musica.



% INGEGNERIA
% LIVORNO
% ANGELO Farina
% EIGENMIKE



%\vfill\null

\begin{figure}[t!]
\begin{center}
\begin{minipage}{.693\textwidth}
  \includegraphics[width=.98\linewidth]{storia/193879_1947881938691_3859395_o-bn.jpeg}
\end{minipage}%
\begin{minipage}{.307\textwidth}
  \includegraphics[width=.98\linewidth]{storia/209721_1947881218673_3705002_o-bn.jpeg}
\end{minipage}
%\vspace{-20pt}
\caption{\textbf{Autoritratto con prospettiva}. AES @Livorno 2011. \\
  Da sinistra: Pasquale Citera, Simone Pappalardo e Giuseppe Silvi. \\
	Angelo Farina presenta il software di registrazione per Eigenmike con ripresa video panoramica integrata.}
\label{storia:aesliv}
\end{center}
\end{figure}


%-------------------------------------------------------------------------------
%--------------------------------------------------------- RAPPORTO DI RICERCA -
%-------------------------------------------------------------------------------

\section*{Rapporto di ricerca}

\begin{quote}
	[I] L'arte musicale consiste nella conoscenza profonda acquisita con
  l'esperienza, della modulazione\footnote{Per il significato assunto dal termine
  \emph{modulatio} (da \emph{modus}, misura) nel linguaggio musicologico
  medievale cfr. AGOSTINO, \emph{De musica}, I, 2, 2 [\ldots] abilità grazie
  alla quale avviene che un qualcosa si muova in maniera conveniente.}  ed ha il
  proprio fondamento nel suono e nel canto. Il termine \emph{musica} trae
  origine dal nome delle Muse, così chiamate \textbf{parole greche}, cioè
  \emph{dall'atto del ricercare}, poiché gli antichi ritenevano che fosse
  necessario il loro aiuto al momento di ricercare la forza espressiva da
  infondere nei carmi e la giusta modulazione della voce. [2] La voce delle
  muse, in quanto oggetto dei sensi, o svanisce con il trascorrere del tempo
  oppure si fissa nella memoria: proprio per questo, dunque, le stesse muse sono
  state immaginate dai poeti figlie di Memoria e Giove. I suoni, infatti, non
  sono trattenuti dall'uomo attraverso la facoltà della memoria, muoiono, poiché
  non possono essere fissati mediante la scrittura.
\end{quote}

% ISIDORO DI SIVIGLIA

%\begin{multicols}{2}

Il primo pensiero sull'altoparlante tetraedrico risale al 2009. Ci fu un rapido
scambio di opinioni con Piero che mi rispose \emph{“nemmeno \mg~ ha mai osato
tanto”}. Era chiaramente un affettuoso modo di incitare il mio lavoro. Nonostante
ciò non andai mai oltre un misero bozzetto, qualche descrizione dei principi
costruttivi e funzionali. Poi arrivò l'anno del cambiamento, il 2013. L'anno si
aprì con la morte di Piero e si chiuse con il pensionamento di \gn, il mio
maestro di sempre. Giorgio mi portò alla musica, al conservatorio, a lui devo
l'infinito presente musicale. Come solo la vita sa fare, con il suo ciclico
mutevole rigenerare ci fu subito una nuova persona ad appassionarsi alla mia
ricerca. \ml, poco dopo essere salito in cattedra a Roma succedendo \gn, rese
possibile, con poche parole e qualche prezioso consiglio, la ripresa dei lavori
di prototipizzazione con un preciso percorso da seguire. Le lezioni del biennio
magistrale erano esaltanti. Eravamo soltanto due studenti e come me anche
Massimo Massimi\index[names]{Massimi, Massimo} stava lavorando ad un progetto
elettroacustico per la diffusione da concerto denominato \emph{Sicomoro}.
Il confronto era continuo, quotidiano. Arrivai a produrre il mio primo esemplare
funzionante nell'ottobre 2014. Contemporaneamente formulai una tecnica microfonica
associata al diffusore, ispirata a quella di \mg~ che chiamai \emph{TETRAREC}.

% \begin{enumerate}
% \item 2009-xx-xx S.T.ONE, appunti e bozzetti, scatola.
% \item 2014-09-xx TETRAREC
% \item 2014-10-22 S.T.ONE
% \item 2015-04-27 S.T.ONE3s
% \item 2014-11-18 S.T.ONE3s FORME D'ARIA
% \item acoustic stone
% \end{enumerate}

Come non lo era quella di \mg, anche questa non è una ricerca focalizzata
esclusivamente su tecniche di diffusione sonora nello spazio. C'è un problema di
fondo che emerge e collega queste ricerche e quelle di chiunque
altro si sieda in un contesto in cui ci siano microfoni e diffusori.

È un problema di fondo, latente, da decenni, fin dal principio delle tecniche
associate al suono riprodotto. È presente anche nel brevetto sulla stereofonia
di Alan Blumlein del 1931-33. Si tratta del prezioso equilibrio acustico
instaurato tra soggetti acustici e di quello difficilmente ottenibile tra questi
ed i sistemi elettroacustici.

\printbibliography
\end{refsection}
