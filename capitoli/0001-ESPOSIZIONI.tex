%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../2020-PTO.tex

%-------------------------------------------------------------------------------
%------------------------------------------------ ESPOSIZIONI ELETTROACUSTICHE -
%-------------------------------------------------------------------------------

% \chapter{Esposizioni\\[-0.5cm]
%          Elettroacustiche\footnote{Appunti per la conferenza del 11 novembre 2019
%          presso Avidi Lumi, Roma.}}

\chapter[Esposizioni Elettroacustiche]{Esposizioni\\[-0.5cm]Elettroacustiche\raisebox{.3\baselineskip}{\normalsize\footnotemark}}
\footnotetext{Appunti per la conferenza del 11 novembre 2019 presso Avidi Lumi, Roma.}
\label{c:ee}

\begin{quote}
  \ldots il dio gli offerse \\
  di scegliersi un premio – una scelta dannosa, \\
  perché Mida ne usò malissimo e disse: “Fa’ in modo \\
  che tutto quello che tocco, si muti in fulvo oro” \\
  Bacco assentì, concedendo un dono nocivo \\
  e rammaricandosi che non avesse scelto di meglio. \\
  Il re berecinzio andò via, tutto contento del suo malanno \\
  e cominciò, toccando qua e là, a verificare la promessa del dio. \\[0.2cm]
  A stento trattiene le sue speranze, immaginandosi un mondo \\
  tutto d’oro: ma mentre esulta, i servi preparano \\
  la mensa imbandita, con pane tostato. \\
  Ma adesso, appena la sua mano toccava i doni di Cerere, \\
  i doni di Cerere si irrigidivano; \ldots \\[0.2cm]
  Attonito per la stranezza dell’inconveniente, povero e ricco, \\
  cerca di sfuggire alla sua ricchezza, odia quello che aveva chiesto. \\[0.2cm]
  \ldots “Perdonami, padre Bacco, ho sbagliato, \\
  ma abbi pietà, ti prego, toglimi a questo bellissimo male!”
  \footnote{Ovidio, Metamorfosi XI, 100-138}.
\end{quote}

Con il paradosso per cui il dono di Bacco aveva reso il re Mida ricco ed allo
stesso tempo così povero da morire di fame ci si proietta diretti nel cuore della
questione straordinaria di cui è l'elettroacustica, come dire, ricchi di un dono
fantastico come l'ascolto, nella sua stratificazione fisica e psicologica di udito
e pensiero, ricchi di competenze tecnologiche e scientifiche, di complessi strumenti
di produzione, analisi ed elaborazione, poveri ed incapaci di comprenderne il
significato ed i meccanismi più semplici.

Tutto suona, il mondo è oro per le nostre orecchie. Ricchi ma poveri. Dotati ma
sordi. Così capaci di meraviglie tecnologiche, con mani fatate mutiamo in oro
anche le peggiori schifezze acustiche, per poi essere completamente incapaci di
ascoltarle, comprenderle e giudicarle.

\begin{quote}
  Il re berecinzio andò via, tutto contento del suo malanno \\
  e cominciò, toccando qua e là, a verificare la promessa del dio\footnote{Ovidio, Metamorfosi XI, 100-138}.
\end{quote}

Così siamo, giratori di pomelli per prodotti luccicanti, ignari del malanno che
portiamo. Usiamo parole fuori dal loro contesto e, con le parole rubate, designificate,
indichiamo oggetti e processi senza apprezzarne il valore reale,
indicando spesso il vuoto. Mastering come andare dal barbiere, compressione come
se avessimo chili di troppo. Di esempi come questi ne avremmo per giorni e giorni
di  chiacchiere ma generalmente si parla poco di visione, con le orecchie,
ascolto compartecipato di orecchie e cervello, di messa in scena dei suoni.

\begin{quote}
  \ldots “Per non rimanere invischiato nell’oro \\
  male desiderato, va’ al fiume vicino a Sardi, \\
  e cammina sul monte, risalendo le acque, \\
  finché arriverai alla sorgente del fiume, \\
  e là metti il capo dove è più forte il getto \\
  della fonte, e lava insieme il corpo e la colpa”\footnote{Ovidio, Metamorfosi XI, 100-138}.
\end{quote}

Così Bacco ci indica il percorso per lavare le nostre colpe: dobbiamo lasciare i
pomelli e mettere il capo nella sorgente, dove è più forte il getto. Di ronte al
bivio potremmo rassegnarci nel dire che la sorgente, elettroacustica, è in un
momento storico, in un luogo, a valle del fluido percorso del tempo, che forse
abbiamo perso. Oppure potremmo accettare semplicemnte che la sorgente è nelle
parole con cui definiamo i conncetti e, tornando ad esse, sotto il peso del
loro forte getto, ci laveremo dai nostri malanni.

Mi è capitato più volte, di dover definire la \emph{Stereofonia} ed osservare
reazioni più o meno in accordo, come se si possano avere opinioni in merito ad
una definizione, come se possano esistere diverse sfumature di \emph{Stereofonia}.
È difficile accettarlo, ma generalmente si possono contemplare opinioni su
definizioni linguistiche, anche contrastanti, perché ci dilettiamo nel fare
confusione con le parole.

% \begin{wrapfigure}[17]{O}{0.48\textwidth}
%   \begin{center}
%     \includegraphics[width=0.45\textwidth]{images/disegni/figa.png}
%     \vspace{-10pt}
%   \end{center}
%   \label{ee:figa}
% \end{wrapfigure}

Il primo passo necessario verso la comprensione del concetto di \emph{Stereofonia},
per di arrivare alle tecniche ed alle tecnologie elettroacustiche che la
rendono possibile, è stabilire attraverso l'etimologia del termine, e dei
termini ad esso collegati, una base concettuale solida. \emph{Stereo}\label{stereo}, dal greco
\emph{Stere\'os}, significa \emph{solido}. Non un numero, non una configurazione
ma un aggettivo qualitativo. Nel dizionario inglese Oxford: \emph{Solid, firm
and stable in shape. Having three dimensions}. Solido, \emph{solid}, dalla radice
latina di \emph{Solidus, Sollus}, intero. Con la parola \emph{Stereofonia} dovremmo quindi descrivere una condizione
nella quale \emph{phon\={e}}, sempre dal greco, \emph{suono}, la \emph{voce},
arrivi all'ascoltatore solida, integra, ferma e stabile nella sua forma (sonora)
multi dimensionale, intera.
\begin{wrapfigure}[18]{O}{0.48\textwidth}
  \begin{center}
    \includegraphics[width=0.45\textwidth]{images/disegni/figb.png}
    \vspace{-10pt}
  \end{center}
  \label{ee:figb}
\end{wrapfigure}
Indispensabile alla comprensione è anche la descrizione del concetto di \emph{mono}, nomignolo di
\emph{monofonico}, espresso nel legame tra \emph{monos} e \emph{phon\={e}}: una voce,
\emph{one voice}, \emph{alone}, sola. La stessa parola usata nella descrizione
del canto gregoriano ad una voce, successivamente evolutasi nella \emph{polifonia}
(dal greco \emph{poluph\={o}nia}, da \emph{polu}, molte e \emph{phon\={e}},
voci). Quindi la dicotomia, se proprio deve essercene una, tra monofonia e
stereofonia semplicemente non esiste. L'estensione del concetto di monofonia,
nel suo eventuale opposto, è polifonia. La stereofonia è semplicemente un
concetto altro.

Con la parola stereofonia dovremmo descrivere anche la condizione in base alla
quale il suono arrivi \emph{solido} all'ascoltatore, \emph{intero, fermo e
stabile} nella sua forma sonora multidimensionale originaria, attraverso
la riproduzione elettroacustica, attraverso la trasmissione e la diffusione mediante
altoparlanti, con un numero qualsiasi, o necessario, di canali. In questo caso,
facendo riferimento alla definizione dell'aggettivo \emph{stereofònico}, ci
si apre alle tecniche ed alle tecnologie che hanno reso possibile la
trasmissione, la riproduzione e la diffusione del suono in stereofonia.
Un aggettivo che dovrebbe essere usato con cautela, nella circostanza in cui la
riproduzione dei suoni avvenga in modo
che l’ascoltatore abbia l’impressione di trovarsi nello spazio sonoro originale o,
nel caso non ve ne sia, per sorgenti di natura non acustica, che restituiscano
informazioni tali da descrivere una correlazione al sistema percettivo, simile a
quella suggerita da sorgenti acustiche.

Condizioni di ascolto stereofoniche, dal latino \emph{auscultare}, prestare
attenzione a qualcosa in quanto oggetto o motivo di informazione, nel caso specifico,
informazioni di stereofonia.

\emph{Una voce in una piccola stanza riverberante è una condizione d'ascolto che
rispetti queste qualità?}

Prima di approfondire questioni di propagazione e percezione del suono, val la pena
dedicare un tempo alla letteratura specializzata.

\begin{quote}
When recording music considerable trouble is experienced with the unpleasant
effects produced by echoes wich in the normal way would not be noticed by anyone
listening in the room in which the performance is taking place. An observer in
the room is listening with two ears, so that echoes reach him with the directional
significance which he associates with the music performed in such room. He,
therefore, discounts these echoes and psychologically focuses his attention on
the source of the sound\footnote{Quando si registra musica acustica, si riscontrano
notevoli problemi a causa degli effetti indesiderati prodotti dalle riflessioni
acustiche dell'ambiente, che nell'ascolto normale non vengono notati dagli
ascoltatori nella stanza in cui si svolge l'esibizione. L'ascoltatore, nella stanza,
ascolta attraverso le due orecchie, le riflessioni lo raggiungano con il significato
direzionale che associa alla musica eseguita in quella stanza.
Pertanto, elimina dal messaggio le informazioni delle riflessioni e focalizza
psicologicamente la sua attenzione sulla sorgente sonora.}.
\end{quote}

Richiesta di brevetto numero 394325 del 14 dicembre 1931, accettazione del 14
giugno 1933. \emph{Alan Dower Blumlein}.

\begin{wrapfigure}[18]{O}{0.48\textwidth}
  \begin{center}
    \includegraphics[width=0.45\textwidth]{images/disegni/figc.png}
    \vspace{-10pt}
  \end{center}
  \label{ee:figc}
\end{wrapfigure}

La risposta alla domanda \emph{una voce in una piccola stanza riverberante è una
condizione d'ascolto che rispetti queste qualità?} è, in funzione di quanto
appena letto, chiaramente affermativa. Anche con un solo oggetto sonoro, una
sola voce, in una piccola stanza, siamo in
presenza di un fenomeno acustico stereofonico. Almeno così dice Blumlein, papà
della stereofonia, nel brevetto in cui ne rende i concetti fondamentali, solidi,
stabili nel tempo e nello spazio delle parole, nel brevetto tecnologico che stabilisce
il \emph{point break} dell'elettroacustica, per il resto dell'umanità.

Una voce nello spazio di una stanzetta si dirige, con una sua direzione, verso
un punto e contemporaneamente, con meno direzionalità, lateralemente, raggiunge
il resto della stanza. Questo meccanismo ha a che fare con la forma sonora di una
voce, prima ancora che con la forma architettonica della piccola stanza.
Possiamo immaginare la forma sonora come un'armatura attorno al nostro oggetto
sonoro, un'armatura fatta di fittissime molecole in vibrazione. Ogni suono ha una
sua veste plastica. Dicendo ottavino e poi contrabbasso voi avrete già
collegato tutto ciò che vi serve per vederli, sentirli, ed ora, volendo, vestirli
della loro forma sonora. Questa si staglia nello spazio circostante e si espande e si muove
all'interno di uno spazio e ne viene modellata come una massa morbida all'interno
di un contenitore. Qui iniziano i fenomeni di riflessione e la forma si
cristallizza assumendo caratteristiche in funzione dello spazio e, quindi, del tempo.
L'ascoltatore che partecipa a questo evento vede una persona solida parlare nello
spazio di una stanza e sente la forma solida della voce provenire dalla sua bocca
e contemporaneamente, quindi subito dopo, dalla stanza sotto forma di riflessioni.
%Sì! converremo infine, questa esperienza di ascolto rispetta queste qualità.

Una voce che attraverso la sua forma acustica riempia uno spazio acustico è
un'esperienza d'ascolto stereofonica. Non è ancora giunto il momento di
interrompere la discussione dicendo: “ma come, non servono due diffusori?” Non
ancora, il problema è più complesso. È importante sottolineare che la stereofonia,
l'ascolto stereofonico, è una qualità dell'ascolto che si può osservare in
determinate circostanze e che richiede necessariamente il lavoro concertato delle
due orecchie. Un ascolto stereofonico è quindi possibile solo in coincidenza
con un ascolto \emph{binaurale}, ovvero effettuato con entrambe le orecchie.

\begin{figure}[t]
\begin{center}
  \includegraphics[width=.98\linewidth]{images/disegni/figd1d2.png}
%\caption{}
\label{ee:figd1d2}
\end{center}
\end{figure}

\begin{quote}
When the music is reproduced through a single channel the echoes arrive from the same direction as the direct sound so that confusion result\footnote{Quando la musica viene riprodotta attraverso un singolo canale, gli echi arrivano dalla stessa direzione del suono diretto in modo tale da creare confusione.}.
\end{quote}

Qui si sviluppa tutta la questione, un solo diffusore non è in grado di rappresentare
la solidità originaria, la forma sonora dell'oggetto acustico originario, il suo
rapporto con lo spazio che lo ha modellato. Per comprendere meglio ogni possibile
questione legata alla diffusione sonora, mediante dispositivi elettroacustici ci
vorrebbe un minimo di tempo speso nella sperimentazione con lo strumento altoparlante.
Perché di questo si parla, di uno strumento tecnico, tecnologico, musicale e
profesisonale.

\begin{quote}
  \ldots il solo Mida \\
  lo criticò e disse che era un’ingiustizia. \\
  Il dio di Delo non sopportò che le sue orecchie \\
  stolide conservassero figura umana, \\
  gliele tirò e allungò, le cosparse di pelame grigio, \\
  le rese instabili alla base, che potessero muoversi. \\
  Il resto è di uomo; la condanna riguarda una sola parte \\
  del corpo – porta le orecchie del tardo asinello.
\end{quote}

La parabola bacchiana si conclude. Noi, che abbiamo
osservato da vicino la maledizione di Bacco, non possiamo più tacere e seminiamo,
il vento muoverà le orecchie penzolanti e porterà le nostre confessioni altrove.

\begin{quote}
  Desidera nasconderle, e per vergogna \\
  si prova a coprire le tempie con una benda di porpora. \\
  Ma il servitore che aveva il compito di tagliargli i capelli \\
  le vide e, non osando svelare quello che aveva visto,\\
  ma pure desiderando di farlo e non riuscendo \\
  a tacere, si appartò e scavò un buco per terra, \\
  e sussurrò a bassa voce alla terra scavata come \\
  aveva visto le orecchie del suo padrone;\\
  poi ricoprì di terra la sua spiata, \\
  ricoprì il buco e se ne andò via in silenzio. \\
  Ma in quel punto cominciò a crescere un fitto \\
  bosco di canne e quando, dopo un anno, fiorirono, \\
  tradirono il seminatore, e mosse dal lieve soffio dell’Austro, \\
  riferirono le parole sepolte e denunciarono le orecchie di Mida.
\end{quote}
