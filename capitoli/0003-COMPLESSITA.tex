%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../2020-PTO.tex

%-------------------------------------------------------------------------------
%------------------------------------------------ ESPOSIZIONI ELETTROACUSTICHE -
%-------------------------------------------------------------------------------

\chapter[Ascoltare la Complessità]{Ascoltare la\\[-0.5cm]Complessità\raisebox{.3\baselineskip}{\normalsize\footnotemark}}
\footnotetext{Appunti per la conferenza del 08 Luglio 2021 per ArteScienza 2023, Gothe Istitut, Roma.}
\label{c:cac}

\emph{Come ascoltiamo la complessità?} %È una domanda che appartiene a questioni
%più ampie di \emph{come percepiamo la complessità} che, con annesse premesse
%di tipo filosofico e psico-cognitivo, comporta ramificazioni di pensiero verso
%una \emph{sinestesia della complessità}.

Lo studio della spettromorfologia dei suoni ha radici profonde nella storia
dell'analisi del suono, nella narrazione dei tratti caratteristici dell'oggetto
sonoro, nel suo svolgimento temporale. Questo workshop pone l'obiettivo di
giungere alla possibilità di integrare quelle conoscenze con tecnologie di
ripresa e diffusione sonora multidimensionale. Ogni fenomeno acustico si sviluppa
mediante una complessa propagazione attraverso lo spazio. Dal punto di vista
dell'ascolto questa complessità combina molteplici elementi, in una relazione
topologica tra forma e spazio, che si manifestano come fonti inestricabili di
informazioni sulla natura dello stesso segnale. Questi sono gli aspetti più
vulnerabili del suono in un concerto elettroacustico. Allo stesso tempo, sono
anche i parametri critici su cui agire per ottenere risultati sonori di pura
magia. Quando le relazioni spazio temporali tra sorgenti acustiche ed
elettroacustiche sono multidimensionalmente equilibrate e l'ascoltatore
proiettato in un altrove contemporaneo, noi siamo maghi.

\clearpage

%La ricerca indaga la varianza topologica della forma sonora acustica nella relazione micro-ritmica con l'ascoltatore attraverso lo spazio. Qual è l'approccio più efficiente per l'analisi della forma sonora di un oggetto acustico? È possibile riprodurre una forma sonora e confrontare la sua riproduzione elettroacustica con quella acustica? Qual è l'impatto emotivo della relazione micro-ritmica tra forme sonore miste durante un concerto dal vivo? La ricerca descrive strategie di analisi quadridimensionale per concettualizzare il profilo sonoro della realtà nella sua epoca di riproduzione virtuale. È l'occasione per esplorare le relazioni micro-ritmiche della percezione della complessità nell'esperienza formale di lunga durata.





Alla profondità di ogni letteratura, il tempo.
Considerando l'ascoltatore come osservatore del tempo, il ruolo della musica come gioco di memoria, come speculazione del compositore sul rapporto tempo-relazione attraverso la mente sconosciuta dell'ascoltatore, cosa succede nello spazio dal gesto esecutivo all'attività di un osservatore? Sono le preziose relazioni temporali da scoprire e spiegare.
La letteratura elettroacustica spiega cos'è la forma-suono, principalmente attraverso concettualizzazioni astratte. Tuttavia, essa è realmente osservabile anche in ambito acustico. È il modo in cui possiamo immaginare il suono del corno e le sue differenze con un fagotto o un diverso spazio riverberato intorno a loro. È la memoria plastica della relazione ritmica, quindi è necessaria una rappresentazione della forma del suono.
Quale approccio all'analisi della forma del suono di un oggetto acustico è più efficiente? L'efficienza della rappresentazione è necessaria per mettere a fuoco la relazione tra questi fondamenti fondamentali e il modo di ascoltare delle persone. Deve essere legata alla reale possibilità di messa in scena, e deve produrre una soluzione tangibile per la musica dal vivo.
È possibile riprodurre una forma sonora e confrontare la sua riproduzione elettroacustica con quella acustica? Sì, è possibile. Il solido tridimensionale più semplice è il tetraedro. La diffusione del suono attraverso le sue quattro facce permette di ottenere la riproduzione della forma sonora più precisa e più efficiente.
Qual è l'impatto emotivo della relazione micro-ritmica tra forme sonore miste durante un concerto dal vivo? È una domanda molto irrisolta. Si potrebbe rispondere con strategie precise per mappare, tracciare e analizzare la musica scritta sullo stesso nucleo di dati osservati. Il palco è la metà dell'attitudine all'ascolto. La ricerca per cucire entrambi.

La ricerca stabilirà una comunità di pensiero intorno a fatti rilevanti che emergono dal lavoro profondo sui fondamenti temporali della percezione.

La musica non è solo composizione. Non è artigianato, né solo un mestiere. La musica è il pensiero.

Queste parole di Luigi Nono spiegano l'attitudine a fare musica con l'inesorabilità della percezione, che implica ascoltare e pensare, scrivere e parlare, e cambiare idee. Queste attività sono il ritmo della composizione, le topologie di un'idea musicale.
La ricerca stabilirà una comunità di pensiero intorno a fatti rilevanti che emergono dal lavoro profondo sui fondamenti del tempo della percezione.

Il mondo, nel suo fluire costante ed incessante nel tempo reale della vita
appare estremamente semplice e comprensibile (di una comprensione mediante
osservazione che attinge inevitabilmente alla bisaccia culturale di
appartenenza). La complessità di ciò che percepiamo si può rivelare solo
attraverso un'indagine profonda. Quando per esempio, per osservare e
comprendere, fermiamo il tempo, formuliamo algoritmi, stabiliamo relazioni.
Passa quindi da semplice a complesso in funzione della volontà di entrare nei
labirinti della comprensione. È semplice tutto ciò che resta fuori dal
labirinto, la porta d'accesso, è complesso tutto ciò che richiede una relazione
tempo-culturale attraverso il labirinto.%, senza necessariamente trovare la via d'iuscita.

Superficialmente, comprendiamo il tempo fino a che non dobbiamo definire il
tempo. E lo comprendiamo meglio solo quando siamo scesi nelle ininite piege del
tempo, il quale ci annuncia che non avevamo capito nulla del tempo.

Comprendiamo l'ascolto ed i fenomeni acustici. Approfondiamo: \emph{siamo capaci
di descriverne il funzionamento e nel caso migliore replicarlo in laboratorio?}
Si alza un sì planetario. Sappiamo molto di come ascoltiamo, sappiamo molto del
canto di un usignolo; potremmo quindi rispondere sì, sapremmo replicare tutto il
sistema in laboratorio e desumere che quella capacità rappresenta, come modello,
tutto l'ascoltabile. Ma è un sì di superficie, una semplificazione tipica del
momento culturale. Il che è facilmente dimostrabile con la successiva domanda:
\emph{siamo in grado di descrivere la complessità di
una variazione timbrica e simultaneamente dinamica di un archetto che sfrega le corde di un violino, dal
\emph{nut} al \emph{ponticello}, da \emph{inclinato} a \emph{piatto}, \emph{leggero}
e poi \emph{pesante}\ldots e lo sappiamo descrivere in un complesso articolatorio
di un suono ogni $500ms$; sappiamo poi descrivere questo comportamento orchestrato
in un quartetto d'archi all'interno di uno spazio riverberante e con il pubblico
in grado di muoversi liberamente?} Ancora si? Lo sappiamo infine riprodurre in laboratorio?
O almeno osservare una pratica efficace per amplificare l'evento acustico in
sala da concerto? Si? Mediante altoparlanti tradizionali che possono suonare
controllati solo sul fronte e solo mediante il parametro dell'ampiezza?

%-------------------------------------------------------------------------------
\subsection*{COMPLESSITÀ}

%\begin{info}[Complessità]
caratteristica di un sistema (perciò detto complesso), concepito come un aggregato
organico e strutturato di parti tra loro ingerenti, in base alla quale il
comportamento globale del sistema non è immediatamente riconducibile a quello
dei singoli costituenti, dipendendo dal modo in cui essi interagiscono. - treccani.it
%\end{info}

Mi si perdonerà quest'indugio enciclopedico, funzionale solo ad evidenziare lo
stato attuale della ricerca per semplificazione attualmente attuata di \emph{default}.

\textbf{Sullo sondo di ogni letteratura, il tempo}.

Il tutto finora esposto ci riconduce all'antica questione se siano le scienze ad
alimentare le arti, o se quest'ultime abbiano una certa forma di libertà che
permette salti e collegamenti impossibili nella visione scientiica. Si può
scegliere da che parte stare, tuttavià l'evidenza sulla quale vorrei puntare il
dito sta nel fatto che le articolazioni musicali di cui sopra sono il mondo semplice
di un compositore contemporaneo (uno: Giorgio Netti, S. Giovanni Rotondo, Puglia)
mentre il mondo scientifico di riferimento (tre: AALTO, Finlandia; Centro RITMO,
Norvegia, UKRI CENTER, Londra) osserva il mondo reale (acustico) con estrema
semplificazione.

Volendo concludere questo argomento con violenza, l'intelligenza artificiale è
in grado di risolvere problematiche complesse laddove lo sguardo umano è ancora
capace di leggere la complessità; nel rapporto con le arti l'intelligenza
artiiciale è allo stato di demenza. È alimentata da semplici ricette liofilizzate
e scolastiche. Per alimentare un pensiero artistico complesso, al pari di un
algoritmo, è nnecessario uno sguardo complesso alla realtà a cui l'arte appartiene.

%-------------------------------------------------------------------------------
\subsubsection*{NICCHIA}

La musica contemporanea è spesso definita come un argomento di nicchia.

Cos'è un argomento di nicchia? Un pensiero di nicchia? È un pensiero comprensibile
solo a pochi? È probabile. Ma non lo sono tutti i pensieri specifici?

Un'equazione di secondo grado appartiene al pensiero globale, così come la
possibilità di un testo in latino. Sono passaggi essenziali della formazione
mentale in età adolescenziale che guarda in nprospettiva al mondo scientifico.
Non l'ascolto e l'analisi di una sequenza di Berio. Non un quadro di Burri.
Così cresciamo, nell'epoca cresciamo la mente matematica, in cui comprendiamo
l'integrale e le derivate, ascoltiamo merda. Giungiamo all'esame di stato senza
sapere chi sia stato Adriano Olivetti, ignorando che viviamo nel mondo di cui
egli stesso è stato start-up.
=======

\emph{Cosa accade alla forma sonora di uno strumento in presenza di tecninche
estese applicate allo strumento?}

Nel 2018 ho avuto la possibilità di ampliicare \emph{ur}, brano per contrabbasso solo,
di Giorgio Netti, con cinnque difusori tetraedrici ed una microfonazione dedicata.
Penso a quei modi, rifletto sulla relazione tra forma e movimento. Una mano che
inizia a produrre suono lì, nello stesso luogo dello strumento dove pochi minuti
prima premeva solo posizioni, (la mano sinistra di \emph{ur} nel secondo
movimento) d'un tratto diventa esplosione di forma acustica e musicale.

Di nuovo una qualità che presuppone dei contenuti coerenti con delle
caratteristiche specifiche. Ora, nel mondo elettroacustico, del suono prodotto
o riprodotto elettricamente, dovremmo essere in grado di effettuare lo stesso
ragionamento sostituendo alla persona che parla un diffusore generico. Come per
l'essere umano, la voce è esempio di suono proprietario anche per il diffusore
si può scegliere un suono che lo caratterizzi elettroacusticamente, un suono
che lo rende particolare: il suono definito rumore rosa. Posizionato il diffusore
nella stessa stanza e con le stesse circostanze di ascolto precedenti, \emph{avremmo
una condizione di ascolto stereofonico?} Ovviamente si. Un solo diffusore può
costituire una condizione d'ascolto stereofonica. In questo caso l'oggetto
acustico è un diffusore che esprime se stesso attraverso un suono non informativo.
Un rumore è caratterizzato da un'assenza di informazione, fatta esclusione del
fatto stesso che è rumore. Questa descirizione di stereofonia possibile anche
con un solo soggetto sonoro, voce o diffusore che sia, non è così comune e
condivisa. Ciò accade a causa del fatto che spesso si fa confusione tra stereofonia,
o stereofonica, come aggettivo applicato alla tecnica di diffusione e registrazione,
piuttosto che alla qualità percettiva che queste tecniche dovrebbero suggerire.
Per arrivare a descrivere la tecnica dobbiamo percorrere ancora alcuni passi.
Nel momento in cui si passa da un dominio puramente acustico sia esso derivante
da una voce umana quanto un rumore diffuso attraverso un altoparlante, ad un
dominio di riproduzione acustica, ovvero di rappresentazione attraverso meccanismi
e tecniche allora cambia completamente lo scenario acustico e le circostanze di
ascolto. \emph{Un diffusore tradizionale può riprodurre una voce umana o un diffusore
che suona rumore rosa?} Si certo che può riprodurli. \emph{Questa riproduzione
costituirebbe un ascolto stereofonico della sorgente originale?} No, non lo sarebbe.
